import os  # TODO: Move to setup
import re
import sqlite3
import time
from datetime import datetime as dt, timedelta
from html.parser import HTMLParser
from random import random

import requests

import cache


class LogListParser(HTMLParser):
    def __init__(self):
        self.columns = ['id', 'type', 'sailors', 'result', 'date']
        self.logs = {}
        self.current_log = {}
        self.pos = 0
        super().__init__()

    def error(self, message):
        pass

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            self.current_log['id'] = attrs[0][1].split('/')[-1]

    def handle_endtag(self, tag):
        if tag == 'tr':
            self.logs[self.current_log['id']] = self.current_log
            self.current_log = {}
            self.pos = 0

    def handle_data(self, data):
        if len(data.strip()) == 0:
            return
        if self.get_starttag_text() is None:
            return
        if self.get_starttag_text().startswith('<td'):
            if self.pos == 2:
                self.current_log['gods'] = [name.strip() for name in data.split(',')]
            elif self.pos == 3:
                self.current_log['treasure'] = int(data.strip()[-1])
            elif self.pos == 4:
                self.current_log['date'] = data
            self.pos += 1


def load_logs(date):
    logs = {}
    seed = random()
    c = cache.Cache()
    end_date = dt.strftime(dt.strptime(date, '%d.%m.%Y') + timedelta(days=1), '%d.%m.%Y')
    r = requests.get('https://gv.erinome.net/duels/log', dict(act='search', t=3, b=date, e=end_date, s=seed))
    log_count = int(re.findall('([0-9]*)\$', r.text)[0])

    for i in range(1, log_count // 20 + 1 if log_count % 20 == 0 else log_count // 20 + 2):
        if r is None:
            r = requests.get('https://gv.erinome.net/duels/log',
                             dict(act='search', t=3, b=date, e=end_date, p=i, s=seed))
        p = LogListParser()
        table_data = r.text[len(str(log_count)) + 1:]
        log_count = int(log_count)
        p.feed(table_data)
        for key in p.logs:
            logs[key] = p.logs[key]
            c.get_log(key)
        r = None
    return logs, log_count


def setup():
    script_dir = os.path.dirname(__file__) + '/../cache/'
    os.makedirs(script_dir, 0x755, True)
    db = sqlite3.connect('../cache/cache.sqlite')
    c = db.cursor()
    res = c.execute('CREATE TABLE IF NOT EXISTS logs ( `id` character(10) PRIMARY KEY,\
                                                            `date` int(8),\
                                                            `gods` varchar(200),\
                                                            `treasure` tinyint)')


def dump_logs():
    start_date = dt.strptime('29.08.2015', '%d.%m.%Y')
    db = sqlite3.connect('../cache/cache.sqlite')
    c = db.cursor()
    try:
        start_date = dt.strptime(str(c.execute('SELECT MAX(date) from logs').fetchall()[0][0]), '%Y%m%d%H%M')
        # start_date = dt.strptime(str(c.execute('SELECT NULL from logs').fetchall()[0][0]), '%Y%m%d%H%M')
    except ValueError:
        pass
    while start_date < dt.now():
        iter_time = dt.now()
        logs, count = load_logs(dt.strftime(start_date, '%d.%m.%Y'))
        for key in logs:
            try:
                c.execute('INSERT INTO logs (`id`, `date`,`gods`,`treasure`) VALUES (?, ?, ?, ?)',
                          (
                              logs[key]['id'],
                              dt.strftime(dt.strptime(logs[key]['date'], '%d.%m.%Y %H:%M'), '%Y%m%d%H%M'),
                              ','.join(logs[key]['gods']),
                              logs[key]['treasure']
                          ))
            except sqlite3.IntegrityError:
                continue
        db.commit()
        start_date += timedelta(days=1)
        print(dt.strftime(start_date, '%d.%m.%Y'), dt.now() - iter_time)
        time.sleep(10)
        continue


if __name__ == '__main__':
    dump_logs()
