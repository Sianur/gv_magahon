import os

import requests


class Cache:
    def __init__(self, file=None):
        # Костыли!
        if file is None:
            self.dir = os.path.dirname(os.path.dirname(__file__)) + '/cache/'
        else:
            self.dir = os.path.dirname(file) + '/cache/'
        self.url = "https://gv.erinome.net/duels/log/"
        os.makedirs(self.dir, 0x755, True)

    def get_log(self, log_id):
        if not os.path.exists(self.dir + str(log_id)):
            success, text = self.download_log(log_id)
            if success:
                return text
            else:
                return None
        else:
            with open(self.dir + log_id, mode='r', encoding='utf-8') as file:
                text = file.read()
                file.close()
            return text

    def download_log(self, log_id):
        r = requests.get(self.url + log_id)
        if r.status_code == 200:
            with open(self.dir + log_id, mode='w', encoding='utf-8') as file:
                file.write(r.text)
                file.close()
        return True, r.text
