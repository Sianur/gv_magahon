import re  # ToDo: move to the tileset

from log_db.cache import Cache
from sea_log.tile_set import TileSet


def print_islands(tile_set):
    for island in tile_set.islands:
        print('====== Island ({}) ======'.format(island.type))
        for tile in island:
            print(tile)


c = Cache(__file__)
text = c.get_log('kw1kd1d')
map_str = re.findall('var m = \[([0-9,]*)\];', text)[0]
tr = re.findall('var tr = (\{[:\"0-9\[\]\-,_]*\})', text)[0]

sea_map = TileSet(map_str)

'''
# prints all found islands
print_islands(sea_map)
'''

'''
# searches for vortexes
vortexes = [tile for tile in sea_map if tile.type == 64]
for i in range(len(vortexes)):
    for j in range(i + 1, len(vortexes)):
        print(vortexes[i] - vortexes[j], vortexes[i].range(vortexes[j]))
'''

# saves map to PNG

for tile in sea_map:
    tile.value = 256 * (tile.sector() % 2)
sea_map.draw_map_png(filename='test.png',
                     borders=(7, 14, 21),
                     radius=22,
                     v_min=0,
                     v_max=10,
                     legend=False
                     )
