import os
import sys

from sea_log.map_hex import MapHex
from sea_log.tile_set import TileSet


def blank_map(r=21):
    ts = TileSet()
    for x in range(-r, r + 1):
        for y in range(-r, r + 1):
            if abs(x + y) <= r:
                ts.tiles.append(MapHex((x, y)))
    return ts


def load_file(filename, **kwargs):
    rewrite = kwargs.get('rw', True)
    data_path = kwargs.get('data_path', '')
    img_path = kwargs.get('img_path', '')
    mode = kwargs.get('mode', 'int')

    if os.path.exists(img_path + filename + '.png') and not rewrite:
        return
    v_max = None
    v_min = None
    tiles = {}
    r = 21
    with open(data_path + filename, encoding='utf-8') as datafile:
        for line in datafile:
            x, y, val = tuple(line.split(','))
            x, y, val = int(x), int(y), float(val)
            v_max = val if v_max is None else max(val, v_max)
            v_min = val if v_min is None else min(val, v_max)
            tiles[(x, y)] = val
            r = max(r, abs(x), abs(y), abs(x + y))
        ts = blank_map(r)
        ts.tile_at(0, 0).colour = 0, 0, 0
        if 0 <= v_min < 0.001 and v_max < 0.025:
            v_min, v_max = 0, 0.025
        if mode == 'int':
            v_min = 1
        v_span = v_max - v_min
        for c in tiles:
            tile = ts.tile_at(c)
            assert isinstance(tile, MapHex)
            if v_span != 0:
                tile.value = int(512 * (tiles[c] - v_min) / v_span)
            else:
                tile.value = None
                tile.colour = 0, 0, 0
        ts.draw_map_png(filename=img_path + filename + '.png',
                        borders=(7, 14, 21),
                        radius=r,
                        v_min=v_min,
                        v_max=v_max,
                        legend=True,
                        legend_mode=mode
                        )
    return


if __name__ == '__main__':
    if len(sys.argv) > 2:
        mode = sys.argv[2]
    else:
        mode = 'float'
    if len(sys.argv) > 1:
        path = os.path.realpath(sys.argv[1])
        if os.path.isdir(path):
            path += '/'
            os.makedirs(path + 'img/', 0x777, True)
            for filename in os.listdir(path):
                if os.path.isdir(path + filename):
                    continue
                if filename.endswith('.png'):
                    continue
                load_file(filename, data_path=path, img_path=path + 'img/', rw=True, mode=mode)
        else:
            filename, path = os.path.basename(path), path[:-len(os.path.basename(path))]
            load_file(filename, data_path=path, img_path=path, rw=True, mode=mode)
    else:
        print('Usage: stat_grapher.py <file|dir> [mode: int|float]')
