from math import sqrt

from PIL import Image, ImageDraw

from .island import Island
from .map_hex import MapHex


class TileSet:
    def __init__(self, data=None):
        if type(data) is str:
            assert isinstance(data, str)
            self.tiles = [MapHex(encoded_tile) for encoded_tile in data.split(',')]
        else:
            self.tiles = []
        self.islands = self.find_islands()

    def find_islands(self):
        islands = []
        seen_island_tiles = []
        for h in self.tiles:
            assert isinstance(h, MapHex)
            if h.island_type() > 0 and h not in seen_island_tiles:
                island = [h]
                seen_island_tiles.append(h)
                l = 0
                while len(island) > l:
                    l = len(island)
                    for island_tile in island:
                        for tile in island_tile.neighbours(self.tiles):
                            assert isinstance(tile, MapHex)
                            if tile.island_type() > 0 and tile not in island:
                                island.append(tile)
                                seen_island_tiles.append(tile)
                islands.append(Island(island))
        return islands

    def __iter__(self):
        return self.tiles.__iter__()

    def draw_map_svg(self, **kwargs):
        filename = kwargs.get('filename', 'test.svg')
        hex_side = 15
        map_r = 22
        map_h = int(hex_side * (2 * map_r + 1) * sqrt(3) + 0.5)
        map_w = 2 * hex_side * (2 * map_r + 1)
        with open(filename, 'w') as outfile:
            print('''<?xml version="1.0" standalone="no"?>
                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                <svg width="%spx" height="%spx" version="1.1" xmlns="http://www.w3.org/2000/svg">''' % (
                map_w, map_h + 30), file=outfile)
            for tile in self.tiles:
                assert isinstance(tile, MapHex)
                tile.value = tile.type * 4
                x, y = tile.cartesian()
                # print(tile, x, y)
                x *= hex_side
                y *= -hex_side
                x += map_w / 2
                y += map_h / 2
                points = [
                    (x, y + hex_side),
                    (x + hex_side * sqrt(3) * 0.5, y + hex_side / 2.0),
                    (x + hex_side * sqrt(3) * 0.5, y - hex_side / 2.0),
                    (x, y - hex_side),
                    (x - hex_side * sqrt(3) * 0.5, y - hex_side / 2.0),
                    (x - hex_side * sqrt(3) * 0.5, y + hex_side / 2.0)
                ]
                # print(points)
                cr, cg, cb = tile.get_colour()
                # print(cr, cg, cb)
                print('<polygon fill="{}" stroke-width="1" stroke="rgb(0, 0, 0)" points="{}" />'.format(
                    'rgb({},{},{})'.format(cr, cg, cb),
                    ' '.join(['{},{}'.format(px, py) for (px, py) in points])
                ),
                    file=outfile
                )
            print('</svg>', file=outfile)
            outfile.close()

    def draw_map_png(self, **kwargs):
        filename = kwargs.get('filename', 'test.svg')
        hex_side = kwargs.get('hex_side', 15)
        borders = kwargs.get('borders', [])
        map_r = kwargs.get('radius', 22)
        legend = kwargs.get('legend', False)
        legend_mode = kwargs.get('legend_mode', 'int')
        v_min = kwargs.get('v_min', 0)
        v_max = kwargs.get('v_max', 0.1)

        map_h = int(hex_side * (2 * map_r + 1) * sqrt(3) + 0.5)
        map_w = 2 * hex_side * (2 * map_r + 1)

        if legend:
            im = Image.new('RGBA', (map_w, map_h + 30), color=(255, 255, 255, 0))
        else:
            im = Image.new('RGBA', (map_w, map_h), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        port_tile = MapHex(0, 0)

        for tile in self.tiles:
            assert isinstance(tile, MapHex)
            x, y = tile.cartesian()
            x *= hex_side
            y *= -hex_side
            x += map_w / 2
            y += map_h / 2
            points = [
                (x, y + hex_side),
                (x + hex_side * sqrt(3) * 0.5, y + hex_side / 2.0),
                (x + hex_side * sqrt(3) * 0.5, y - hex_side / 2.0),
                (x, y - hex_side),
                (x - hex_side * sqrt(3) * 0.5, y - hex_side / 2.0),
                (x - hex_side * sqrt(3) * 0.5, y + hex_side / 2.0)
            ]
            cr, cg, cb = tile.get_colour()
            draw.polygon(points, fill=(cr, cg, cb), outline='black')

        for tile in self.tiles:
            if port_tile.range(tile) in borders:
                x, y = tile.cartesian()
                x *= hex_side
                y *= -hex_side
                x += map_w / 2
                y += map_h / 2
                points = [
                    (x, y + hex_side),
                    (x + hex_side * sqrt(3) * 0.5, y + hex_side / 2.0),
                    (x + hex_side * sqrt(3) * 0.5, y - hex_side / 2.0),
                    (x, y - hex_side),
                    (x - hex_side * sqrt(3) * 0.5, y - hex_side / 2.0),
                    (x - hex_side * sqrt(3) * 0.5, y + hex_side / 2.0)
                ]
                draw.line(points + [points[0]], 'black', width=int(hex_side / 5. + 0.5))

        # border_colors = ['yellow', 'orange', 'red', 'green', 'blue']
        # for i in range(len(borders)):
        #     border = borders[i] * sqrt(3)
        #     x, y = map_w / 2, map_h / 2
        #     points = [
        #         (x + hex_side * border, y),
        #         (x + hex_side * border / 2.0, y + hex_side * border * sqrt(3) * 0.5),
        #         (x - hex_side * border / 2.0, y + hex_side * border * sqrt(3) * 0.5),
        #         (x - hex_side * border, y),
        #         (x - hex_side * border / 2.0, y - hex_side * border * sqrt(3) * 0.5),
        #         (x + hex_side * border / 2.0, y - hex_side * border * sqrt(3) * 0.5),
        #     ]
        #     for j in range(len(points)):
        #         draw.line(points + [points[0]],
        #                   fill=border_colors[i % len(border_colors)],
        #                   width=int(hex_side / 5 + 0.5))

        if legend:
            # draw.rectangle([map_w//2 - 275, map_h, map_w//2 + 276, map_h + 19], fill='white')
            for i in range(513):
                x = map_w // 2 - 256 + i
                if legend_mode == 'int':
                    l_step = 1
                    while (v_max - v_min) / l_step > 8:
                        l_step += 1
                    p_step = int(512 / l_step)
                    if i % p_step == 0:
                        draw.line([x, map_h + 15, x, map_h + 19], fill='white')
                        draw.text([x - 5, map_h + 5], '{}'.format(int(v_min + (v_max - v_min) * i / p_step / l_step)),
                                  fill='white')

                else:
                    if i % 64 == 0:
                        draw.line([x, map_h + 15, x, map_h + 19], fill='white')
                        draw.text([x - 5, map_h + 5], '{0:.3g}%'.format(i / 511. * float(v_max - v_min) * 100.),
                                  fill='white')
                draw.line([x, map_h + 29, x, map_h + 20], fill=port_tile.get_colour(i))

        im.save(filename, format='PNG')

    def tile_at(self, x, y=None):
        if type(x) == tuple:
            x, y = x
        for tile in self.tiles:
            if tile.x == x and tile.y == y:
                return tile
        return None
