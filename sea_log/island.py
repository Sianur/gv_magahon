from .map_hex import MapHex


class Island:
    def __init__(self, tiles=None, **kwargs):
        self.type = kwargs.get('type', -1)
        if tiles is None:
            self.tiles = []
        else:
            self.tiles = tiles
        self.check_type()

    def range(self, other):
        if type(other) == MapHex:
            assert isinstance(other, MapHex)
            ranges = [other.range(tile) for tile in self.tiles]
            return min(ranges) - 1
        elif type(other) == Island:
            assert isinstance(other, Island)
            ranges = []
            for other_tile in other.tiles:
                assert isinstance(other_tile, MapHex)
                ranges += [other_tile.range(tile) for tile in self.tiles]
            return min(ranges) - 1
        else:
            return -1

    def check_type(self):
        self.type = -1
        for tile in self.tiles:
            if self.type in [-1, 73]:
                self.type = tile.type

    def __iter__(self):
        return list.__iter__(self.tiles)
