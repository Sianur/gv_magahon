from math import sqrt


class MapHex:
    types_desc = {
        32: 'пусто',
        33: 'красная метка',
        35: 'граница',
        38: 'голубая метка',
        40: 'синяя метка',
        41: 'фиолетовая метка',
        44: 'риф',
        60: 'остров с едой',
        62: 'остров с подсказкой',
        63: 'не открыто',
        64: 'водоворот',
        66: 'блуждающая тварь',
        70: 'самка',
        71: 'клад с острова',
        73: 'исследованный остров',
        77: 'самец',
        91: 'подсказка на 11',
        93: 'поблизости клада нет',
        94: 'зелёная метка',
        95: '???',
        96: 'оранжевая метка',
        97: 'стрелка на З ←',
        98: 'стационарная тварь',
        99: 'стрелка на ЮВ ↘',
        100: 'стрелка на В →',
        101: 'стрелка на СВ ↗',
        103: 'клад из рыбы',
        105: 'остров с вопросиком',
        109: 'маяк',
        110: 'остров с ремонтом',
        111: 'подсказка на 9',
        112: 'порт',
        113: 'стрелка на СЗ ↖',
        116: 'подсказка на 3',
        117: 'подсказка на 7',
        118: 'остров с молилкой',
        119: 'стрелка на С ↑',
        120: 'стрелка на Ю ↓',
        121: 'подсказка на 5',
        122: 'стрелка на ЮЗ ↙',
        126: 'жёлтая метка'
    }

    def __init__(self, *args, **kwargs):
        self.type = kwargs.get('type', -1)
        self.value = kwargs.get('value', None)
        self.colour = kwargs.get('colour', None)
        if len(args) == 1:
            if type(args[0]) in [tuple, list]:
                # Ячейка задаётся парой [x, y] или (x, y)
                self.x = int(args[0][0])
                self.y = int(args[0][1])
            elif type(args[0]) in [int, str]:
                # Ячейку надо декодировать из формата демиургов:
                d = MapHex.gv_decode(args[0])
                self.x, self.y, self.type = d.x, d.y, d.type
        elif len(args) >= 2:
            if type(args[0]) == int and type(args[1]) == int:
                self.x = int(args[0])
                self.y = int(args[1])
        else:
            self.x = 0
            self.y = 0
        self.z = -(self.x + self.y)

    @staticmethod
    def gv_decode(source):
        """
        декодирует ячейку из формата демиургов
        4-байтовое целое, с конца: x, y, z = - (x + y), тип
        каждый параметр - типа signed char

        :param source: int|float|str
        :return: MapHex
        """
        try:
            data = int(source)
        except ValueError:
            return 0, 0, 0, -1
        decoded = []
        for pos in range(4):
            decoded.append((data // pow(256, pos)) & 255)
            if decoded[pos] > 127:
                decoded[pos] -= 256
        return MapHex(decoded[0], decoded[1], type=decoded[3])

    def __eq__(self, other):
        """
        Полное совпадение клеток - координаты и тип

        :param other: MapHex
        :return: Boolean
        """
        return self.x == other.x and self.y == other.y and self.type == other.type

    def __sub__(self, other):
        """
        Возвращает вектор между двумя клетками карты

        :param other: MapHex
        :return: MapHex
        """
        new_x = self.x - other.x
        new_y = self.y - other.y
        return MapHex(new_x, new_y)

    def __add__(self, other):
        """
        Для работы с векторными сдвигами
        Вектор хранится аналогично клетке, с типом -1 (по умолчанию)
        Сумма клеток тоже работает, однако не имеет смысла.

        :param other: MapHex
        :return: MapHex
        """
        new_x = self.x + other.x
        new_y = self.y + other.y
        return MapHex(new_x, new_y)

    def __str__(self) -> str:
        if self.type != -1:
            return '({}, {}, {}): {}'.format(self.x, self.y, self.z, self.type)
        return '({}, {}, {})'.format(self.x, self.y, self.z)

    def range(self, other) -> int:
        d = self - other
        return max([abs(d.x), abs(d.y), abs(d.x + d.y)])

    def island_type(self) -> int:
        if self.type in [71, 73, 77]:  # исследованный остров
            return 73
        elif self.type == 60:
            return 60
        elif self.type == 62:
            return 62
        elif self.type == 105:
            return 105
        elif self.type == 109:  # Lighthouse
            return 0
        elif self.type == 110:
            return 110
        elif self.type == 118:
            return 118
        else:
            return 0

    def neighbours(self, tile_set: list) -> list:
        assert isinstance(self, MapHex)
        assert isinstance(tile_set, list)
        return [tile_2 for tile_2 in tile_set if self.range(tile_2) == 1]

    def sector(self):
        if self.x > 0:
            if self.y > 0:
                return 0
            elif self.y == 0:
                return 1
            elif self.x + self.y > 0:
                return 2
            elif self.x + self.y == 0:
                return 3
            else:
                return 4
        elif self.x == 0:
            if self.y < 0:
                return 5
            elif self.y > 0:
                return 11
            else:
                return 13
        else:
            if self.y < 0:
                return 6
            elif self.y == 0:
                return 7
            elif self.x + self.y > 0:
                return 10
            elif self.x + self.y == 0:
                return 9
            else:
                return 8

    def cartesian(self):
        return (self.x - self.y) * sqrt(3) * 0.5, (self.x + self.y) * 1.5

    def get_colour(self, value=None):
        colours_span = ((255, 255, 255, 0), (0, 0, 255, 512), (255, 64, 64, 511))
        if value is None:
            value = self.value
        if value is not None:
            value = int(max(min(value, 511), 0))
        if self.colour is not None:
            return self.colour
        if value is None:
            return 255, 255, 255
        elif value < colours_span[1][3]:
            span = float(colours_span[1][3] - colours_span[0][3])
            r, g, b = colours_span[0][0] + int((colours_span[1][0] - colours_span[0][0]) * value / span), \
                      colours_span[0][1] + int((colours_span[1][1] - colours_span[0][1]) * value / span), \
                      colours_span[0][2] + int((colours_span[1][2] - colours_span[0][2]) * value / span)

            return r, g, b
        else:
            span = float(colours_span[2][3] - colours_span[1][3])
            offset = colours_span[1][3]
            r, g, b = colours_span[1][0] + int((colours_span[2][0] - colours_span[1][0]) * (value - offset) / span), \
                      colours_span[1][1] + int((colours_span[2][1] - colours_span[1][1]) * (value - offset) / span), \
                      colours_span[1][2] + int((colours_span[2][2] - colours_span[1][2]) * (value - offset) / span)

            return r, g, b
